package com.hiberus.gm.core;

import com.hiberus.gm.data.Producto;

public final class ProductUtils {

	//metodo 0
	public static float obtenerPesoEnGramos(Producto producto) {
		return producto.getPeso() * 1000;
	}
	
	
	//metodo 1
	public static float volumenProducto_CARLOS(Producto producto) {
		return producto.getAltura() * producto.getAnchura() * producto.getProfundidad();
	}
	
	//metodo 2
	public static float volumenProducto2_2(Producto producto) {
		return producto.getAltura() * 2 * producto.getAnchura() * producto.getProfundidad();
	}
	
	//metodo 3
	public static float volumenProducto3(Producto producto) {
		return producto.getAltura() * producto.getAnchura() * producto.getProfundidad();
	}
	
	//metodo 4
	
	//metodo 5
	public static float volumenProducto_JP(Producto producto) {
		return producto.getAltura() * producto.getAnchura() * producto.getProfundidad();
	}
	
	//metodo 6
	
	//metodo 7
	
	//metodo 8
	
	//metodo 9
	
	//metodo 10
	
	//metodo 11
	
	//metodo 12
	
	//metodo 13
	
	//metodo 14
	
	//metodo 15
	
	//metodo 16
}
